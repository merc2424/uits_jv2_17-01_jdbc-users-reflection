/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author Merc
 */
public class Main {

    static boolean initialized = true;
    static Connection connection;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");   //загрузка драйвера
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/test",
                    "root",
                    ""
            );
        } catch (ClassNotFoundException | SQLException d) {
            initialized = false;
        }
    }

    public static void main(String[] args) throws Exception {
        String QueryFirstName = "SELECT firstname FROM users";
        String QueryLastName = "SELECT lastname FROM users";
        String QueryEmail = "SELECT email FROM users";

        if (initialized) {
            System.out.println("Connection established!");
            System.out.println("Get all users");
            System.out.println("---------------------------------------------------------------------------");
            getAllUsers();
            System.out.println("");
            System.out.println("===========================================================================");

            System.out.println("Get firstname from users DB");
            System.out.println("");

            printArray(getColumnAsArray(QueryFirstName));

            System.out.println("---------------------------------------------------------------------------");
            System.out.println("Get lastname from users DB");
            System.out.println();

            printArray(getColumnAsArray(QueryLastName));

            System.out.println("---------------------------------------------------------------------------");
            System.out.println("Get email from users DB");
            System.out.println();

            printArray(getColumnAsArray(QueryEmail));

            System.out.println("===========================================================================");
            System.out.println();

            System.out.println("Get users as array");

            printUsers(getArrayOfUsers());
            
            System.out.println("===========================================================================");
            System.out.println();

            System.out.println("Get users as array via Reflection");
            
            
            printUsers(getArrayOfUsersViaReflection());

        } else {
            System.out.println("No connection!");
        }

    }

    static User[] getArrayOfUsers() throws SQLException {    //Достаём массив юзеров из таблицы users
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM users");
        result.last();      //выставляем курсор на последний элемент
        int rowCount = result.getRow();      //получаем колличество записей
        User[] users = new User[rowCount];
        result.beforeFirst();

        for (int i = 0; result.next(); i++) {     //заполняем массив 
            users[i] = new User();
            users[i].setFirstName(result.getString(2));
            users[i].setLastName(result.getString(3)); 
            users[i].setEmail(result.getString(4));
        }  
        return users;
    }
    
    static User[] getArrayOfUsersViaReflection() throws Exception {    //Достаём массив юзеров из таблицы users через рефлексию
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM users");
        result.last();      //выставляем курсор на последний элемент
        int rowCount = result.getRow();      //получаем колличество записей
        User[] users = new User[rowCount];
        result.beforeFirst();

        for (int i = 0; result.next(); i++) {     //заполняем массив 
          
          users[i] = (User) UserBuilderFactory.UserBuilderReflection(
                    User.class, 
                    result.getString(2),
                    result.getString(3),
                    result.getString(4));            
        }  
        return users;
    }
    
    
    

    static String[] getColumnAsArray(String query) throws SQLException {  //получаем поле и записываем его в массив
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(query);

        result.last();
        int rowCount = result.getRow();
        String[] array = new String[rowCount];
        result.beforeFirst();

        for (int i = 0; result.next(); i++) {
            array[i] = result.getString(1);
        }

        return array;
    }

    static void getAllUsers() throws SQLException {       // Достаём всю талицу из БД и выводим на экран
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM users");
        while (result.next()) {
            System.out.print(result.getString(1));
            System.out.print(" | ");
            System.out.print(result.getString(2));
            System.out.print(" | ");
            System.out.print(result.getString(3));
            System.out.print(" | ");
            System.out.println(result.getString(4));
        }
    }

    static void printUsers(User[] users) {  //Красиво печатает массив объектов User
        for (User user : users) {
            System.out.println(user.toString());
        }
    }

    static void printArray(String[] strArray) {  
        for (String string : strArray) {
            System.out.println(string);
        }
    }
  

}
