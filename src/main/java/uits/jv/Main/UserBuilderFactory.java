/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uits.jv.Main;

import java.lang.reflect.Method;


/**
 *
 * @author Merc
 */
public class UserBuilderFactory {

    public static Object UserBuilderReflection(Class c, String firstName, String lastName, String email) throws Exception {

        Class mClassObject = Class.forName(c.getName());

        Object object = mClassObject.newInstance();

        Method methodF = mClassObject.getMethod("setFirstName", String.class);
        methodF.invoke(object, firstName);

        Method methodL = mClassObject.getMethod("setLastName", String.class);
        methodL.invoke(object, lastName);

        Method methodE = mClassObject.getMethod("setEmail", String.class);
        methodE.invoke(object, email);

        return object;
    }
}
